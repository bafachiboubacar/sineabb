package ne.mha.sinea.pem;

//import java.sql.Date;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.nomenclature.typeAmenagement.TypeAmenagement;
import ne.mha.sinea.nomenclature.typeUsage.TypeUsage;

@Data
@Entity
@Table(name = "forage")
public class Forage extends PEM {

	@Column(name = "date_demarrage_foration")
	private Date dateDemarrageForation;
	@Column(name = "date_fin_foration")
	private Date dateFinForation;
	@Column(name = "profondeur_totale_foree")
	private Double profondeurTotaleForee;
	@Column(name = "forage_positif")
	private Boolean foragePositif;
	@Column(name = "profondeur_totale_equipee")
	private Double profondeurTotaleEquipee;
	@Column(name = "profondeur_niveau_statique")
	private Double profondeurNiveauStatique;
	@Column(name = "date_mesure")
	private Date dateMesure;
	@Column(name = "developpement_effectue")
	private Boolean developpementEffectue;
	@Column(name = "debit_au_soufflage")
	private Double debitAuSoufflage;
	@Column(name = "essai_simplifie_duree_pompage")
	private Double essaiSimplifieDureePompage;
	@Column(name = "essai_simplifie_rabattement_mesure")
	private Double essaiSimplifieRabattementMesure;
	@Column(name = "essai_simplifie_debit_maximum_estime")
	private Double essaiSimplifieDebitMaximumEstime;
	@Column(name = "essai_longue_duree_duree_pompage")
	private Double essaiLongueDureeDureePompage;
	@Column(name = "essai_longue_duree_rabattement_mesure")
	private Double essaiLongueDureeRabattementMesure;
	@Column(name = "essai_longue_duree_debit_maximum_estime")
	private Double essaiLongueDureeDebitMaximumEstime;
	@JoinColumn(name = "code_type_usage", referencedColumnName = "code")
	@ManyToOne
	private TypeUsage typeUsage;
	
	@JoinColumn(name = "code_type_amenagement", referencedColumnName = "code")
	@ManyToOne
	private TypeAmenagement typeAmenagement;
	
	@OneToMany(mappedBy="pem")
	private List<EquipementForage> equipementForages = new ArrayList<>();
	
	@OneToMany(mappedBy="pem")
	private List<Piezometre> piezometres = new ArrayList<>();
	
	@OneToMany(mappedBy="pem")
	protected List<PompeInstalle> pompeInstalles = new ArrayList<>();
	
}
