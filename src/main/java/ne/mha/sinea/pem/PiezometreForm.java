package ne.mha.sinea.pem;

import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PiezometreForm {

	private Date dateObservation;
	private String heureObservation;
	private Double profondeurPiezo;
	private Double hauteurPiezo;
}
