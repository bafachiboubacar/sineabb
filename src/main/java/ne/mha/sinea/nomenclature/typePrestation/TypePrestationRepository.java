package ne.mha.sinea.nomenclature.typePrestation;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypePrestationRepository extends CrudRepository<TypePrestation, Integer> {
	TypePrestation findByCode(Integer code);
	TypePrestation findByIsDeletedFalseAndLibelle(String libelle);
	List<TypePrestation> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypePrestation> findByIsDeletedFalse();
	List<TypePrestation> findByIsDeletedTrue();
	List<TypePrestation> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypePrestation> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
