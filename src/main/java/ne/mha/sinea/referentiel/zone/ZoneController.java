package ne.mha.sinea.referentiel.zone;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class ZoneController {
	@Autowired
	ZoneRepository zoneService;
	
	//@PreAuthorize("hasAuthority('gestion des zones')")
	@GetMapping("/zone")
	public String zone(Model model){
		List<Zone> zones = zoneService.findByIsDeletedFalseOrderByLibelleAsc();
		model.addAttribute("zones", zones);
		
		model.addAttribute("horizontalMenu", "horizontalMenu");
		model.addAttribute("sidebarMenu", "configurationSidebarMenu");
		model.addAttribute("breadcrumb", "breadcrumb");
		model.addAttribute("navigationPath", "Referentiel/Découpage Administratif");
		model.addAttribute("viewPath", "referentiel/zone/zone");
		return Template.defaultTemplate;
	}
}
