package ne.mha.sinea.pem;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PuitsInfosGeneralesForm {
	private String numeroIRH;
	private Date anneeRealisation;
	private Double latitude;
	private Double longitude;
	private Integer codeFinancement;
	private Integer codeTypePuits;
	private Integer codeProjet;
	private Integer codeLocalite;
	private Double hauteurMargelle;
	private String autreInfoMargelle;
	private boolean typePuits;
	private boolean amenagementSurface;
	private boolean antibourbier;
	private boolean canalEvacuationEtPuitsPerdu;
	private boolean aireAssainie;
	private boolean murCloture;
	private boolean couvercle;
	private boolean puisette;
	private boolean fourche;
	private boolean developpementEffectueAuCuffat;
	private boolean couleurEau;
	private boolean trousseCoupante;
	private boolean dalleFond;
	private boolean pompeImmergee;
	private Date dateMesure;
	private Integer codeTypeExhaure;
	private Integer codeEtatOuvrage;
	private boolean potabiliteEau;
	private Double ESCIEHDureePompage;
	private Double ESCIEHRabattementMesure;
	private Double ESCIEHDebitMaximumEstime;
	private Integer abreuvoirs;
	private Integer portique;
	private Double profondeurTotale;
	private Double profondeurCuvelee;
	private Double diametreCuvelage;
	private Double profondeurNiveauStatique;
	private Double diametreCaptage;
	private Integer nombreBusesCaptage;
	private Double profondeurSommetColonneCaptage;
	private Double hauteurCaptage;
	private Double hauteurRecouvrement;
	private Double hauteurMassifFiltrant;
	private String renseignementDivers;
}
