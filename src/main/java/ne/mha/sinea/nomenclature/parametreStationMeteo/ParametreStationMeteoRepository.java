package ne.mha.sinea.nomenclature.parametreStationMeteo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface ParametreStationMeteoRepository extends CrudRepository<ParametreStationMeteo, Integer> {
	ParametreStationMeteo findByCode(Integer code);
	ParametreStationMeteo findByIsDeletedFalseAndLibelle(String libelle);
	List<ParametreStationMeteo> findByIsDeletedFalseOrderByLibelleAsc();
	List<ParametreStationMeteo> findByIsDeletedFalse();
	List<ParametreStationMeteo> findByIsDeletedTrue();
	List<ParametreStationMeteo> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<ParametreStationMeteo> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
