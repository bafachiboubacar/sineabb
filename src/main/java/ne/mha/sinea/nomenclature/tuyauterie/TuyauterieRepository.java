package ne.mha.sinea.nomenclature.tuyauterie;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TuyauterieRepository extends CrudRepository<Tuyauterie, Integer> {
	Tuyauterie findByCode(Integer code);
	Tuyauterie findByIsDeletedFalseAndLibelle(String libelle);
	List<Tuyauterie> findByIsDeletedFalseOrderByLibelleAsc();
	List<Tuyauterie> findByIsDeletedFalse();
	List<Tuyauterie> findByIsDeletedTrue();
	List<Tuyauterie> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Tuyauterie> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
