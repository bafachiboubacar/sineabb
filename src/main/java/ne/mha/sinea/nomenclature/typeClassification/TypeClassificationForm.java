package ne.mha.sinea.nomenclature.typeClassification;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeClassificationForm {

	private int code;
	private String libelle;
	
}
