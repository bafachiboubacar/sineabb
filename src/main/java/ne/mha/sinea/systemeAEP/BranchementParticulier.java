package ne.mha.sinea.systemeAEP;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.lieu.Lieu;

@Data
@Entity
@Table(name = "branchement_particulier")
public class BranchementParticulier extends CommonProperties {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	protected Integer code;
	@Column(name = "code_ins")
	protected String codeINS;
	@JoinColumn(name = "code_lieu", referencedColumnName = "code")
	@ManyToOne
	private Lieu lieu;
	@Column(name = "nom_lieu")
	private String nomLieu;
	@Column(name = "compteur_installe")
	private boolean compteurInstalle;
	@Column(name = "latitude")
	protected Double latitude;
	@Column(name = "longitude")
	protected Double longitude;
	@JoinColumn(name = "code_systeme_aep", referencedColumnName = "code")
	@ManyToOne
	protected SystemeAEP systemeAEP;
}
