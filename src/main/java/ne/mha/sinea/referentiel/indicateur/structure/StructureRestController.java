package ne.mha.sinea.referentiel.indicateur.structure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class StructureRestController {

	@Autowired
	StructureRepository structureService;
	
	@PreAuthorize("hasAuthority('gestion des structures')")
	@PostMapping("/structure")
    public int addStructure(@Validated StructureForm structureForm,BindingResult bindingResult, Model model) {
		Structure savedStructure = new Structure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				Structure P = new Structure();
				P.setLibelle(structureForm.getLibelle());
				savedStructure = structureService.save(P);
				
				
				}
			catch(Exception e){
				
				
					}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedStructure.getCode();
		
        
    }
	
	@PreAuthorize("hasAuthority('gestion des structures')")
	@PostMapping("/updateStructure")
    public int updateStructure(@Validated StructureForm structureForm,BindingResult bindingResult, Model model) {
		Structure savedStructure = new Structure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				Structure P = structureService.findByCode(structureForm.getCode());
				P.setLibelle(structureForm.getLibelle());
				savedStructure = structureService.save(P);
				
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedStructure.getCode();
		
        
    }
	
	@PreAuthorize("hasAuthority('gestion des structures')")
	@PostMapping("/deleteStructure")
    public int deleteStructure(@Validated StructureForm structureForm,BindingResult bindingResult, Model model) {
		Structure savedStructure = new Structure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
				Structure P = structureService.findByCode(structureForm.getCode());
				P.setDeleted(true);
				savedStructure = structureService.save(P);
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedStructure.getCode();
		
        
    }
}
