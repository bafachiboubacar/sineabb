package ne.mha.sinea.nomenclature.financement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class FinancementRestController {

	@Autowired
	FinancementRepository financementService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/financement")
	public int addFinancement(@Validated FinancementForm financementForm,BindingResult bindingResult, Model model) {
		Financement savedFinancement = new Financement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Financement P = new Financement();
					P.setLibelle(financementForm.getLibelle());
					savedFinancement = financementService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedFinancement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateFinancement")
    public int updateFinancement(@Validated FinancementForm financementForm,BindingResult bindingResult, Model model) {
		Financement savedFinancement = new Financement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Financement P = financementService.findByCode(financementForm.getCode());
					P.setLibelle(financementForm.getLibelle());
					savedFinancement = financementService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedFinancement.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteFinancement")
    public int deleteFinancement(@Validated FinancementForm financementForm,BindingResult bindingResult, Model model) {
		Financement savedFinancement = new Financement();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Financement P = financementService.findByCode(financementForm.getCode());
					P.setDeleted(true);
					savedFinancement = financementService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedFinancement.getCode();
		
        
    }
}
