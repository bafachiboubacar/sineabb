package ne.mha.sinea.pem;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface PompeInstalleRepository extends CrudRepository<PompeInstalle, Integer> {
	
	PompeInstalle findByCode(Integer code);
	List<PompeInstalle> findByIsDeletedFalse();
	
}