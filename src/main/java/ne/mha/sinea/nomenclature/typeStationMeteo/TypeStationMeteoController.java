package ne.mha.sinea.nomenclature.typeStationMeteo;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeStationMeteoController {

	@Autowired
	TypeStationMeteoRepository typeStationMeteoService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Station Meteo')")
	@GetMapping("/typeStationMeteo")
	public String  addTypeStationMeteo(TypeStationMeteoForm typeStationMeteoForm, Model model) {
		try{
			List<TypeStationMeteo> typeStationMeteos = typeStationMeteoService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeStationMeteos", typeStationMeteos);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Type Station Meteo");
			model.addAttribute("viewPath", "nomenclature/typeStationMeteo/typeStationMeteo");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
