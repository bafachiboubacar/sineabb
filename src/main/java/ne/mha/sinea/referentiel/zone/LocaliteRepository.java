package ne.mha.sinea.referentiel.zone;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface LocaliteRepository extends CrudRepository<Localite, Integer> {
	Localite findByCode(Integer code);
	Localite findByLibelle(String libelle);
	Localite findByIsDeletedFalseAndLibelle(String libelle);
	List<Localite> findByIsDeletedFalse();
	List<Localite> findByIsDeletedTrue();
	List<Localite> findByIsDeletedFalseAndCommune_Code(Integer code);
	List<Localite> findByIsDeletedFalseAndTypeLocalite_Code(Integer code);
	List<Localite> findByIsDeletedFalseAndTypeLocalite_Libelle(String libelle);
	List<Localite> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Localite> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
}
