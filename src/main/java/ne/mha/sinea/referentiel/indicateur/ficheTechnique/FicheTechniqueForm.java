package ne.mha.sinea.referentiel.indicateur.ficheTechnique;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FicheTechniqueForm {

		private String definition;
		private String donneesRequises;
		private String methodeCalcul;
		private String commentaireLimite;
		private String periodicite;
		private String niveauDesagregation;
		private String unite;
		private String source;
		private String methodeCollecte;
			
}
