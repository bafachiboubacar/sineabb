package ne.mha.sinea.referentiel.indicateur.classification;
/*
 * c'est la table qui va permettre de stocker l'arborescence 
 * des objectifs de développement, des secteurs, des institution, des thèmes
 * en vue de la classification des indicateurs
 * 
 */
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
//nom de la table
@Table(name = "classification")
public class Classification extends CommonProperties {
	// annotation pour indiquier l'identifiant d'une table
	@Id
	//définir un attribut autoincrément
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	@Column(name = "libelle",columnDefinition="TEXT")
	private String libelle;
	//relation reflexive côté fils => classification parent
	@JoinColumn(name = "parent_code", referencedColumnName = "code")
	@ManyToOne
    private Classification parent;
	//annotation pour une relation de type 1-N
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private List<Classification> sous_classifications = new ArrayList<>();
    //niveau pour avoir une idéee de la position de l'élément dans la hiérarchie / arborescence
    @Column(name = "niveau")
    private int niveau;
    //type de classification (objectif, secteur, thème, etc)
    @JoinColumn(name = "code_type_classification", referencedColumnName = "code")
	@ManyToOne
	private TypeClassification typeClassification;

}
