package ne.mha.sinea.nomenclature.typeAmenagement;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeAmenagementRepository extends CrudRepository<TypeAmenagement, Integer> {
	TypeAmenagement findByCode(Integer code);
	TypeAmenagement findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeAmenagement> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeAmenagement> findByIsDeletedFalse();
	List<TypeAmenagement> findByIsDeletedTrue();
	List<TypeAmenagement> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeAmenagement> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
