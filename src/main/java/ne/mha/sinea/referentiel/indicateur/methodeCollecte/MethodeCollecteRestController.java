package ne.mha.sinea.referentiel.indicateur.methodeCollecte;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class MethodeCollecteRestController {

	@Autowired
	MethodeCollecteRepository methodeCollecteService;
	//@PreAuthorize("hasAuthority('gestion des méthodes de collecte')")
	@PostMapping("/methodeCollecte")
	public int addMethodeCollecte(@Validated MethodeCollecteForm methodeCollecteForm,BindingResult bindingResult, Model model) {
		MethodeCollecte savedMethodeCollecte = new MethodeCollecte();
		if (!bindingResult.hasErrors()) {
			
			try {
					MethodeCollecte P = new MethodeCollecte();
					P.setLibelle(methodeCollecteForm.getLibelle());
					savedMethodeCollecte = methodeCollecteService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMethodeCollecte.getCode();
		
        
    }
	//@PreAuthorize("hasAuthority('gestion des méthodes de collecte')")
	@PostMapping("/updateMethodeCollecte")
    public int updateMethodeCollecte(@Validated MethodeCollecteForm methodeCollecteForm,BindingResult bindingResult, Model model) {
		MethodeCollecte savedMethodeCollecte = new MethodeCollecte();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MethodeCollecte P = methodeCollecteService.findByCode(methodeCollecteForm.getCode());
					P.setLibelle(methodeCollecteForm.getLibelle());
					savedMethodeCollecte = methodeCollecteService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMethodeCollecte.getCode();
		
        
    }
	//@PreAuthorize("hasAuthority('gestion des méthodes de collecte')")
	@PostMapping("/deleteMethodeCollecte")
    public int deleteMethodeCollecte(@Validated MethodeCollecteForm methodeCollecteForm,BindingResult bindingResult, Model model) {
		MethodeCollecte savedMethodeCollecte = new MethodeCollecte();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					MethodeCollecte P = methodeCollecteService.findByCode(methodeCollecteForm.getCode());
					P.setDeleted(true);
					savedMethodeCollecte = methodeCollecteService.save(P);
				
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMethodeCollecte.getCode();
		
        
    }
}
