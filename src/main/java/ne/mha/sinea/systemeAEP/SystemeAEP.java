package ne.mha.sinea.systemeAEP;

//import java.sql.Date;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.etatOuvrage.EtatOuvrage;
import ne.mha.sinea.nomenclature.financement.Financement;
import ne.mha.sinea.nomenclature.propriete.Propriete;
import ne.mha.sinea.nomenclature.typeSystemeAEP.TypeSystemeAEP;
import ne.mha.sinea.referentiel.projet.Projet;
import ne.mha.sinea.referentiel.zone.Localite;

@Data
@Entity
@Table(name = "systeme_aep")
public class SystemeAEP extends CommonProperties{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@Column(name = "numero_irh")
	private String numeroIRH;
	@Column(name = "nom_aep")
	private String nomAEP;
	@Column(name = "annee_realisation")
	private Date anneeRealisation;
	@JoinColumn(name = "code_financement", referencedColumnName = "code")
	@ManyToOne
	private Financement financement;
	@JoinColumn(name = "code_projet", referencedColumnName = "code")
	@ManyToOne
	private Projet projet;
	@JoinColumn(name = "code_localite", referencedColumnName = "code")
	@ManyToOne
	private Localite localite;
	
	@Column(name = "source_energie_solaire")
	private boolean sourceEnergieSolaire;
	@Column(name = "source_energie_thermique")
	private boolean sourceEnergieThermique;
	@Column(name = "source_energie_reseau_electrique")
	private boolean sourceEnergieReseauElectrique;
	@Column(name = "source_energie_eolienne")
	private boolean sourceEnergieEolienne;
	@Column(name = "source_energie_autre")
	private boolean sourceEnergieAutres;
	@Column(name = "precision_energie_autres")
	private String precisionEnergieAutres;
	
	@Column(name = "source_principale_solaire")
	private boolean sourcePrincipaleSolaire;
	@Column(name = "source_principale_thermique")
	private boolean sourcePrincipaleThermique;
	@Column(name = "source_principale_reseau_electrique")
	private boolean sourcePrincipaleReseauElectrique;
	@Column(name = "source_principale_eolienne")
	private boolean sourcePrincipaleEolienne;
	@Column(name = "source_principale_autre")
	private boolean sourcePrincipaleAutres;
	@Column(name = "precision_source_principale_autres")
	private String precisionSourcePrincipaleAutres;
	@JoinColumn(name = "code_etat_ouvrage", referencedColumnName = "code")
	@ManyToOne
	protected EtatOuvrage etatOuvrage;
	
	@JoinColumn(name = "code_type_systeme_aep", referencedColumnName = "code")
	@ManyToOne
	private TypeSystemeAEP typeSystemeAEP;
	@JoinColumn(name = "code_propriete", referencedColumnName = "code")
	@ManyToOne
	private Propriete propriete;
	
}
