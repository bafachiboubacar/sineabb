package ne.mha.sinea.referentiel.indicateur.sousGroupeIndicateur;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;
import ne.mha.sinea.referentiel.indicateur.sousGroupe.SousGroupe;
import ne.mha.sinea.referentiel.indicateur.sousGroupe.SousGroupeRepository;

@RestController
@ResponseBody
public class SousGroupeIndicateurRestController {

	@Autowired
	SousGroupeIndicateurRepository sousGroupeIndicateurService;
	
	@Autowired
	IndicateurRepository indicateurService;
	
	@Autowired
	SousGroupeRepository sousGroupeService;
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@GetMapping("/listeSousGroupesIndicateur/{code}")
	public String  listeSousGroupesIndicateur(@PathVariable("code") int code) {
		String res = "";
		try{
			List<SousGroupeIndicateur> sousGroupeIndicateur = sousGroupeIndicateurService.findByIsDeletedFalseAndIndicateur_Code(code);
			for(int i=0;i<sousGroupeIndicateur.size();i++)
			{
				if(i==0)
					res += "{ id:"+sousGroupeIndicateur.get(i).getSousGroupe().getCode()+", name:\""+sousGroupeIndicateur.get(i).getSousGroupe().getLibelle()+"\"}";
				else
					res += ",{ id:"+sousGroupeIndicateur.get(i).getSousGroupe().getCode()+", name:\""+sousGroupeIndicateur.get(i).getSousGroupe().getLibelle()+"\"}";
				
			}
			res = "["+res+"]";

			}
		catch(Exception e){
			System.out.println(e);
			}
		return res;
	}
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@GetMapping("/getSousGroupeIndicateurDisponibles/{code}")
	public String  getSousGroupeIndicateurDisponibles(@PathVariable("code") int code) {
		String res = "";
		try{
			List<SousGroupe> sousGroupesDisponibles = new ArrayList<SousGroupe>();
			List<Integer> sousGroupesUtilises = new ArrayList<Integer>();
			List<SousGroupeIndicateur> sousGroupeIndicateur = sousGroupeIndicateurService.findByIsDeletedFalseAndIndicateur_Code(code);
			for(int i=0;i<sousGroupeIndicateur.size();i++)
			{
				sousGroupesUtilises.add(sousGroupeIndicateur.get(i).getSousGroupe().getCode());
			}
			if(sousGroupesUtilises.isEmpty())
				sousGroupesDisponibles = sousGroupeService.findByIsDeletedFalse();
			else
				sousGroupesDisponibles = sousGroupeService.findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(sousGroupesUtilises);
			for(int i=0;i<sousGroupesDisponibles.size();i++)
			{
				if(i==0)
					res += "{ id:"+sousGroupesDisponibles.get(i).getCode()+", pId:0, name:\""+sousGroupesDisponibles.get(i).getLibelle()+"\"}";
				else
					res += ",{ id:"+sousGroupesDisponibles.get(i).getCode()+", pId:0, name:\""+sousGroupesDisponibles.get(i).getLibelle()+"\"}";
				
			}
			if(sousGroupesDisponibles.size() >0)
				res = "[{ id:0, pId:0, name:\"/\", open:true},"+res+"]";
			else
				res = "[{ id:0, pId:0, name:\"/\", open:true}]";
			
			}
		catch(Exception e){
			System.out.println(e);
			}
		return res;
	}
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@PostMapping("/ajoutSousGroupeIndicateur")
	public String ajoutSousGroupeIndicateurSubmit(@RequestParam(value="params[]") Integer[] params,Integer indicateurId) {
		try{
				Indicateur indicateur = indicateurService.findByCode(indicateurId); 
				for(int i=0;i<params.length;i++)
				{
					SousGroupe sousGroupe = sousGroupeService.findByCode(params[i]);
					SousGroupeIndicateur io = new SousGroupeIndicateur();
					io.setCode(new SousGroupeIndicateurKey(indicateur.getCode(),sousGroupe.getCode()));
					io.setIndicateur(indicateur);
					io.setSousGroupe(sousGroupe);
					sousGroupeIndicateurService.save(io);
					
				}
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@PostMapping("/suppressionSousGroupeIndicateur")
	public String suppressionSousGroupeIndicateurSubmit(@RequestParam(value="params[]") Integer[] params,Integer indicateurId) {
		try{
				for(int i=0;i<params.length;i++)
				{
					SousGroupeIndicateur io = sousGroupeIndicateurService.findByIsDeletedFalseAndSousGroupe_CodeAndIndicateur_Code(params[i],indicateurId);
					sousGroupeIndicateurService.delete(io);
					
				}
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	
}
