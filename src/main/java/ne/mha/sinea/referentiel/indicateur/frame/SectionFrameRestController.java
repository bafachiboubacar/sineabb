package ne.mha.sinea.referentiel.indicateur.frame;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
@RestController
@ResponseBody
public class SectionFrameRestController {

	@Autowired
	SectionFrameRepository sectionFrameService;
	//@PreAuthorize("hasAuthority('gestion des sections de saisie')")
	@PostMapping("/ajoutSectionFrame")
	public String ajoutDonneeIndicateurSubmit(@RequestParam(value="nom_frame") String nom_frame,@RequestParam(value="source_data[]") Integer[] source_data,@RequestParam(value="indicateur_data[]") Integer[] indicateur_data,@RequestParam(value="zone_data[]") Integer[] zone_data,@RequestParam(value="periode_data[]") Integer[] periode_data) {
		try{
				List<Integer> sources = Arrays.asList(source_data);
				List<Integer> indicateurs = Arrays.asList(indicateur_data);
				List<Integer> zones = Arrays.asList(zone_data);
				List<Integer> periodes = Arrays.asList(periode_data);
				
				SectionFrame sectionFrame = new SectionFrame();
				sectionFrame.setSources(sources);
				sectionFrame.setIndicateurs(indicateurs);
				sectionFrame.setZones(zones);
				sectionFrame.setPeriodes(periodes);
				sectionFrame.setLibelle(nom_frame);
				
				sectionFrameService.save(sectionFrame);
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	//@PreAuthorize("hasAuthority('gestion des sections de saisie')")
	@PostMapping("/modSectionFrame")
	public String modDonneeIndicateurSubmit(@RequestParam(value="code_frame") Integer code_frame,@RequestParam(value="source_data[]") Integer[] source_data,@RequestParam(value="indicateur_data[]") Integer[] indicateur_data,@RequestParam(value="zone_data[]") Integer[] zone_data,@RequestParam(value="periode_data[]") Integer[] periode_data) {
		try{
				List<Integer> sources = Arrays.asList(source_data);
				List<Integer> indicateurs = Arrays.asList(indicateur_data);
				List<Integer> zones = Arrays.asList(zone_data);
				List<Integer> periodes = Arrays.asList(periode_data);
				
				SectionFrame sectionFrame_old = sectionFrameService.findByCode(code_frame);
				SectionFrame sectionFrame = new SectionFrame();
				sectionFrame.setLibelle(sectionFrame_old.getLibelle());
				sectionFrame.setSources(sources);
				sectionFrame.setIndicateurs(indicateurs);
				sectionFrame.setZones(zones);
				sectionFrame.setPeriodes(periodes);
				
				sectionFrameService.save(sectionFrame);
				sectionFrameService.delete(sectionFrame_old);
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}

}
