package ne.mha.sinea.nomenclature.materiaux;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MateriauxForm {

	private int code;
	private String libelle;
	
}
