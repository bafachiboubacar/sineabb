package ne.mha.sinea.nomenclature.typeRessource;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeRessourceRepository extends CrudRepository<TypeRessource, Integer> {
	TypeRessource findByCode(Integer code);
	TypeRessource findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeRessource> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeRessource> findByIsDeletedFalse();
	List<TypeRessource> findByIsDeletedTrue();
	List<TypeRessource> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeRessource> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
