package ne.mha.sinea.referentiel.indicateur.uniteIndicateur;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface UniteIndicateurRepository extends CrudRepository<UniteIndicateur, UniteIndicateurKey>  {

	List<UniteIndicateur> findByIsDeletedFalse();
	List<UniteIndicateur> findByIsDeletedTrue();
	List<UniteIndicateur> findByIsDeletedFalseAndIndicateur_Code(Integer code);
	UniteIndicateur findByIsDeletedFalseAndUnite_CodeAndIndicateur_Code(Integer codeIndicateur, Integer codeUnite);
	
}
