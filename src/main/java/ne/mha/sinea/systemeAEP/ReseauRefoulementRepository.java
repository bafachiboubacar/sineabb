package ne.mha.sinea.systemeAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;


public interface ReseauRefoulementRepository extends CrudRepository<ReseauRefoulement, Integer> {
	
	ReseauRefoulement findByCode(Integer code);
	List<ReseauRefoulement> findBySystemeAEP_NumeroIRH(String IRH);
	
}