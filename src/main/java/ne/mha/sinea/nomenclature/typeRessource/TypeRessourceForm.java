package ne.mha.sinea.nomenclature.typeRessource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeRessourceForm {

	private int code;
	private String libelle;
	
}
