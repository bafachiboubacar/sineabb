package ne.mha.sinea.systemeAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface CaptageSystemeAEPRepository extends CrudRepository<CaptageSystemeAEP, Integer> {
	
	CaptageSystemeAEP findByCode(Integer code);
	List<CaptageSystemeAEP> findBySystemeAEP_NumeroIRH(String IRH);

}