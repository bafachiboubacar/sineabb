package ne.mha.sinea.nomenclature.disposition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DispositionForm {

	private int code;
	private String libelle;
	
}
