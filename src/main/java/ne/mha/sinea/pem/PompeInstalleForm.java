package ne.mha.sinea.pem;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PompeInstalleForm {

	private Integer codeModelePompe;
	private Integer codeMarquePompe;
	private Date dateInstallationPompe;
}
