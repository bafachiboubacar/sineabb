package ne.mha.sinea.nomenclature.typeTube;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeTubeRepository extends CrudRepository<TypeTube, Integer> {
	TypeTube findByCode(Integer code);
	TypeTube findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeTube> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeTube> findByIsDeletedFalse();
	List<TypeTube> findByIsDeletedTrue();
	List<TypeTube> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeTube> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
