package ne.mha.sinea.systemeAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;


public interface BranchementParticulierRepository extends CrudRepository<BranchementParticulier, Integer> {
	
	BranchementParticulier findByCode(Integer code);
	List<BranchementParticulier> findBySystemeAEP_NumeroIRH(String IRH);

}