package ne.mha.sinea.nomenclature.typeUsage;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeUsageRepository extends CrudRepository<TypeUsage, Integer> {
	TypeUsage findByCode(Integer code);
	TypeUsage findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeUsage> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeUsage> findByIsDeletedFalse();
	List<TypeUsage> findByIsDeletedTrue();
	List<TypeUsage> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeUsage> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
