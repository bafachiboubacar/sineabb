package ne.mha.sinea.pem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Ouvrage {
	private String nomOuvrage;
	private String typeOuvrage;
	private String dateMiseEnService;
	private String etatActuel;
	private String latitude;
	private String longitude;
	private String lien;
}
