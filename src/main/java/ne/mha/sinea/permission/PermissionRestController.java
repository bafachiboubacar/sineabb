package ne.mha.sinea.permission;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
public class PermissionRestController {

	@Autowired
	RoleRepository roleService;
	
	@Autowired
	PrivilegeRepository privilegeService;
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@GetMapping("/getFonctionaliteAutorise/{code}")
	public String  getFonctionaliteAutorise(@PathVariable("code") int code) {
		String res = "";
		try{
			Role role = roleService.findByCode(code);
			List<Privilege> privileges = role.getPrivileges();
			for(int i=0;i<privileges.size();i++)
			{
				if(i==0)
					res += "{ id:"+privileges.get(i).getCode()+", pId:0, name:\""+privileges.get(i).getLibelle()+"\"}";
				else
					res += ",{ id:"+privileges.get(i).getCode()+", pId:0, name:\""+privileges.get(i).getLibelle()+"\"}";
				
			}
			res = "["+res+"]";

			}
		catch(Exception e){
			System.out.println(e);
			}
		return res;
	}
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@GetMapping("/getFonctionaliteDisponible/{code}")
	public String  getIndicateursClassificationDisponibles(@PathVariable("code") int code) {
		String res = "";
		try{
			List<Privilege> fonctionaliteDisponible = new ArrayList<Privilege>();
			List<Integer> fonctionaliteAutorise = new ArrayList<Integer>();
			
			Role role = roleService.findByCode(code);
			List<Privilege> privileges = role.getPrivileges();
			
			for(int i=0;i<privileges.size();i++)
			{
				fonctionaliteAutorise.add(privileges.get(i).getCode());
			}
			if(fonctionaliteAutorise.isEmpty())
				fonctionaliteDisponible = privilegeService.findByIsDeletedFalseOrderByLibelleAsc();
			else
				fonctionaliteDisponible = privilegeService.findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(fonctionaliteAutorise);
			for(int i=0;i<fonctionaliteDisponible.size();i++)
			{
				if(i==0)
					res += "{ id:"+fonctionaliteDisponible.get(i).getCode()+", pId:0, name:\""+fonctionaliteDisponible.get(i).getLibelle()+"\"}";
				else
					res += ",{ id:"+fonctionaliteDisponible.get(i).getCode()+", pId:0, name:\""+fonctionaliteDisponible.get(i).getLibelle()+"\"}";
				
			}
			if(fonctionaliteDisponible.size() >0)
				res = "[{ id:0, pId:0, name:\"/\", open:true},"+res+"]";
			else
				res = "[{ id:0, pId:0, name:\"/\", open:true}]";
			
			}
		catch(Exception e){
			System.out.println(e);
			}
		return res;
	}
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@PostMapping("/ajoutPrivilegeRole")
	public String ajoutIndicateurClassificationSubmit(@RequestParam(value="params[]") Integer[] params,Integer roleId) {
		try{
				Role role = roleService.findByCode(roleId); 
				List<Privilege> privileges = role.getPrivileges();
				for(int i=0;i<params.length;i++)
				{
					Privilege privilege = privilegeService.findByCode(params[i]);
					privileges.add(privilege);
					
				}
				roleService.save(role);
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	
	@PreAuthorize("hasAuthority('gestion des accès')")
	@PostMapping("/suppressionPrivilegeRole")
	public String suppressionIndicateurClassificationSubmit(@RequestParam(value="params[]") Integer[] params,Integer roleId) {
		try{
				Role role = roleService.findByCode(roleId); 
				List<Privilege> privileges = role.getPrivileges();
				for(int i=0;i<params.length;i++)
				{
					Privilege privilege = privilegeService.findByCode(params[i]);
					privileges.remove(privilege);
					
				}
				roleService.save(role);
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	
}
