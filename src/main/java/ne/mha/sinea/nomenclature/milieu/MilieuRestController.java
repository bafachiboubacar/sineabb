package ne.mha.sinea.nomenclature.milieu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class MilieuRestController {

	@Autowired
	MilieuRepository milieuService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/milieu")
	public int addMilieu(@Validated MilieuForm milieuForm,BindingResult bindingResult, Model model) {
		Milieu savedMilieu = new Milieu();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Milieu P = new Milieu();
					P.setLibelle(milieuForm.getLibelle());
					savedMilieu = milieuService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMilieu.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateMilieu")
    public int updateMilieu(@Validated MilieuForm milieuForm,BindingResult bindingResult, Model model) {
		Milieu savedMilieu = new Milieu();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Milieu P = milieuService.findByCode(milieuForm.getCode());
					P.setLibelle(milieuForm.getLibelle());
					savedMilieu = milieuService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMilieu.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteMilieu")
    public int deleteMilieu(@Validated MilieuForm milieuForm,BindingResult bindingResult, Model model) {
		Milieu savedMilieu = new Milieu();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Milieu P = milieuService.findByCode(milieuForm.getCode());
					P.setDeleted(true);
					savedMilieu = milieuService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedMilieu.getCode();
		
        
    }
}
