package ne.mha.sinea.nomenclature.hydraulique;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface HydrauliqueRepository extends CrudRepository<Hydraulique, Integer> {
	Hydraulique findByCode(Integer code);
	Hydraulique findByIsDeletedFalseAndLibelle(String libelle);
	List<Hydraulique> findByIsDeletedFalseOrderByLibelleAsc();
	List<Hydraulique> findByIsDeletedFalse();
	List<Hydraulique> findByIsDeletedTrue();
	List<Hydraulique> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Hydraulique> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
