package ne.mha.sinea.nomenclature.typeTube;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeTubeRestController {

	@Autowired
	TypeTubeRepository typeTubeService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeTube")
	public int addTypeTube(@Validated TypeTubeForm typeTubeForm,BindingResult bindingResult, Model model) {
		TypeTube savedTypeTube = new TypeTube();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeTube P = new TypeTube();
					P.setLibelle(typeTubeForm.getLibelle());
					savedTypeTube = typeTubeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeTube.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeTube")
    public int updateTypeTube(@Validated TypeTubeForm typeTubeForm,BindingResult bindingResult, Model model) {
		TypeTube savedTypeTube = new TypeTube();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeTube P = typeTubeService.findByCode(typeTubeForm.getCode());
					P.setLibelle(typeTubeForm.getLibelle());
					savedTypeTube = typeTubeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeTube.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeTube")
    public int deleteTypeTube(@Validated TypeTubeForm typeTubeForm,BindingResult bindingResult, Model model) {
		TypeTube savedTypeTube = new TypeTube();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeTube P = typeTubeService.findByCode(typeTubeForm.getCode());
					P.setDeleted(true);
					savedTypeTube = typeTubeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeTube.getCode();
		
        
    }
}
