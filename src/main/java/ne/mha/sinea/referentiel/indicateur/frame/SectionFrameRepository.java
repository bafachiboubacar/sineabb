package ne.mha.sinea.referentiel.indicateur.frame;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface SectionFrameRepository extends CrudRepository<SectionFrame, Integer>  {

	List<SectionFrame> findByIsDeletedFalse();
	List<SectionFrame> findByIsDeletedTrue();
	List<SectionFrame> findByIsDeletedFalseOrderByLibelleAsc();
	SectionFrame findByCode(Integer code);
	
}
