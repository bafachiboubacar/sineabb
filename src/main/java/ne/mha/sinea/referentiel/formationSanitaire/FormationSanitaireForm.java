package ne.mha.sinea.referentiel.formationSanitaire;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FormationSanitaireForm {

	private String denomination;
	private double latitude;
	private double longitude;
	private int anneeCreation;
	private Integer codeTypeFormationSanitaire;
	private Integer codeLocalite;
}
