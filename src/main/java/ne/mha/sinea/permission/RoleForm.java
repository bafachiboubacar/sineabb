package ne.mha.sinea.permission;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleForm {

	private int code;
	private String libelle;
	
}
